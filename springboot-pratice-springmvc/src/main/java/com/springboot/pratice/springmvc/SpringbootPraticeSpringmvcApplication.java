package com.springboot.pratice.springmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPraticeSpringmvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPraticeSpringmvcApplication.class, args);
	}

}
