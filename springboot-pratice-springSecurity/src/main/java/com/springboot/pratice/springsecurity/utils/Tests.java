package com.springboot.pratice.springsecurity.utils;

import java.util.Scanner;

/**
 * @author daisy
 * @date 2022-11-03 14:48
 * Copyright ©2022 ctwing
 */
public class Tests {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int a, b;
        while (cin.hasNextInt()) {
            a = cin.nextInt();
            b = cin.nextInt();
            System.out.println(a + b);
        }
    }
}
