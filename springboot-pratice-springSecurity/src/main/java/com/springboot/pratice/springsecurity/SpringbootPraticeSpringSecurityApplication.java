package com.springboot.pratice.springsecurity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@Slf4j
@com.springboot.pratice.common.annotation.EnableCommonConfig
@SpringBootApplication
public class SpringbootPraticeSpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPraticeSpringSecurityApplication.class, args);
		log.info(">>>>>>Started success...");
	}

}
