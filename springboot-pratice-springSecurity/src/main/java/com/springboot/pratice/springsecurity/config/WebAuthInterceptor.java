//package com.springboot.pratice.springsecurity.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Optional;
//
///**
// * @author daisy
// * @date 2020/5/13 20:34
// */
//@Slf4j
//public class WebAuthInterceptor implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        // huo
//        Optional<HandlerMethod> methodOP = getHandlerMethod(handler);
//        // 如果不是访问接口，直接成功
//        if (!methodOP.isPresent()) {
//            return true;
//        }
//
//        HandlerMethod method = methodOP.get();
//
//        preMethodHandle(request, response, method);
//
//        Optional<AccountIdentifier> IdentifierOp;
//
//        String token = this.getToken(request);
//        // 如果有token，则验证有效性
//        if (StringUtils.isNotBlank(token)) {
//            token = decodeToken(token);
//            // 校验和获取用户
//            IdentifierOp = verifyAndGetIdentifier(token, request, response, method);
//            if (IdentifierOp.isPresent()) {
//                postGetAccount(request, response, IdentifierOp.get(), token, method);
//            }
//            // 获取旧的用户
//        } else {
//            IdentifierOp = Optional.empty();
//            // 构建新得用户
//        }
//
//        // 需要授权
//        if (isNeedToAuth(method)) {
//            if (!IdentifierOp.isPresent()) {
//                if (logger.isErrorEnabled()) {
//                    logger.error("接口权限验证失败, 未登录!");
//                }
//                // 未登录异常
//                unAuthorizedProcess(token, request, response, method);
//            }
//            NeedLogin needLogin = method.getMethodAnnotation(NeedLogin.class);
//            if (StringUtils.isNotEmpty(needLogin.permissionSpel())) {
//                SpelExcetor spelExcetor = this.getSpelExcetor();
//                boolean hasPermission = spelExcetor.doneInSpringContext(needLogin.permissionSpel());
//                if (!hasPermission) {
//                    if (logger.isErrorEnabled()) {
//                        logger.error("接口权限验证失败, 权限不足");
//                    }
//                    // 权限不足
//                    forbiddenProcess(token, request, response, method);
//                }
//            } else if (needLogin.permissions().length > 0) {
//                List<Permission> permissions = getNeedPermissions(IdentifierOp.get(), needLogin.permissions(), method);
//                if (!verifyPermissions(IdentifierOp.get(), permissions, request, response)) {
//                    if (logger.isErrorEnabled()) {
//                        logger.error(
//                                "接口权限验证失败, 需要权限: {}",
//                                Joiner.on(",").join(permissions.stream().map(Permission::getDesc).collect(Collectors.toList()))
//                        );
//                    }
//                    // 权限不足
//                    forbiddenProcess(token, request, response, method);
//                }
//            } else if (needLogin.enableInterfacePermission()) {
//                String alias = needLogin.alias();
//                if (StringUtils.isEmpty(alias)) {
//                    alias = method.getMethod().getName();
//                }
//                Permission permission = getNeedPermissions(IdentifierOp.get(), alias, method);
//                if (permission != null && !verifyPermissions(IdentifierOp.get(), Collections.singletonList(permission), request, response)) {
//                    if (logger.isErrorEnabled()) {
//                        logger.error("接口权限验证失败, 需要权限: {}", permission.getDesc());
//                    }
//                    // 权限不足
//                    forbiddenProcess(token, request, response, method);
//                }
//            }
//            if (logger.isDebugEnabled()) {
//                logger.debug("接口权限验证成功:[{}].", request.getServletPath());
//            }
//
//        } else {
//            if (logger.isDebugEnabled()) {
//                logger.debug("接口不需要授权:[{}].", request.getServletPath());
//            }
//        }
//
//        return true;
//
//    }
//
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//    }
//
//    protected abstract void preMethodHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod method);
//
//    protected abstract String getToken(HttpServletRequest request);
//
//    protected abstract SpelExcetor getSpelExcetor();
//
//    /**
//     * 解密token
//     *
//     * @param token
//     * @return
//     */
//    protected String decodeToken(String token) {
//        return token;
//    }
//
//    /**
//     * 校验token并获取用户信息
//     *
//     * @param token
//     * @param request
//     * @param response
//     * @param handler
//     * @return
//     */
//    protected abstract Optional<AccountIdentifier> verifyAndGetIdentifier(String token, HttpServletRequest request, HttpServletResponse response, HandlerMethod handler);
//
//    /**
//     * 未登录流程
//     *
//     * @param token
//     * @param request
//     * @param response
//     * @param handler
//     */
//    protected abstract void unAuthorizedProcess(String token, HttpServletRequest request, HttpServletResponse response, HandlerMethod handler);
//
//    /**
//     * 获取所需权限
//     *
//     * @param permissionIDs
//     * @return
//     */
//    protected abstract List<Permission> getNeedPermissions(AccountIdentifier identifier, int[] permissionIDs, HandlerMethod handler);
//
//    /**
//     * 获取所需权限
//     *
//     * @param permissionName
//     * @return
//     */
//    protected abstract Permission getNeedPermissions(AccountIdentifier identifier, String permissionName, HandlerMethod handler);
//
//    /**
//     * 校验权限
//     *
//     * @param permissions
//     * @param request
//     * @param response
//     * @return
//     */
//    protected abstract boolean verifyPermissions(AccountIdentifier identifier, List<Permission> permissions, HttpServletRequest request, HttpServletResponse response);
//
//    /**
//     * 拒绝流程
//     *
//     * @param token
//     * @param request
//     * @param response
//     * @param handler
//     */
//    protected abstract void forbiddenProcess(String token, HttpServletRequest request, HttpServletResponse response, HandlerMethod handler);
//
//
//    /**
//     * 获取账户后
//     *
//     * @param token
//     * @param request
//     * @param response
//     * @param handler
//     */
//    protected void postGetAccount(HttpServletRequest request, HttpServletResponse response, AccountIdentifier identifier, String token, HandlerMethod handler) {
//
//    }
//
//    /**
//     * 判断是否是Controller方法
//     *
//     * @param handler
//     * @return
//     */
//    private Optional<HandlerMethod> getHandlerMethod(Object handler) {
//        if (handler instanceof HandlerMethod) {
//            return Optional.of((HandlerMethod) handler);
//        }
//        return Optional.empty();
//    }
//
//    /**
//     * 是否需要授权
//     *
//     * @param handler Controller接口方法
//     * @return 是否需要授权
//     */
//    protected boolean isNeedToAuth(HandlerMethod handler) {
//        return handler.hasMethodAnnotation(NeedLogin.class);
//    }
//
//}
//
//}
