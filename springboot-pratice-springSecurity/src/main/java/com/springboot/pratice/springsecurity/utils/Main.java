package com.springboot.pratice.springsecurity.utils;

import java.util.*;

/**
 * @author daisy
 * @date 2022-11-03 14:48
 * Copyright ©2022 ctwing
 */
public class Main {
    /**
     * 第一行输入数量N
     * 第二行输入N个整数M1, M2,……Mn，用空隔隔开，回车打印每个整树的阶乘
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        Scanner cin = new Scanner(System.in);
        int m = cin.nextInt();
        int count=0;

        List<Long> resultList = new ArrayList<>();
        while (cin.hasNextInt()) {
            count++;

            int n = cin.nextInt();
            long result = main.jiecheng(n);
            resultList.add(result);

            if(m==count){
                break;
            }
        }
        for(Long e: resultList){
            String str = e.toString();
//            System.out.println(str.length());
            System.out.println(str + "->" + str.length());
        }
    }
    public long jiecheng(int n){
        if(n==1){
            return 1;
        }else{
            return n*jiecheng(n-1);
        }
    }
}
