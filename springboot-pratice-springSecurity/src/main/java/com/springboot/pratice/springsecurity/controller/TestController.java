package com.springboot.pratice.springsecurity.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author daisy
 * @date 2020/5/13 20:19
 */

@RestController
@RequestMapping("test")
public class TestController {

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello(HttpServletRequest request){
        String reqUrl = request.getRequestURL().toString();
        String reqUri = request.getRequestURI().toString();
        System.out.println("hello,demo，"+ reqUrl);
        System.out.println("hello,demo，"+ reqUri);
        HashMap map = new HashMap();
        map.put("reqUrl",reqUrl);
        map.put("reqUri",reqUri);
        return map.toString();
    }
}
