package com.springboot.pratice.common.config.weixin;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 微信相关配置
 * @author yanglingyu
 * @date 2021/11/12
 */
@Configuration
@Data
public class WeiXinConfig {

    /**
     * 请求微信api的accessToken获取url
     */
    @Value("${wx.access-token-url}")
    private String wxAccessTokenUrl;

    /**
     * grantType
     */
    @Value("${wx.grant-type}")
    private String grantType;

    /**
     * 请求微信接口的sessionUrl
     */
    @Value("${wx.session-url}")
    private String sessionUrl;

    /**
     * 用于小程序审核登录C端手机
     */
    @Value("${wx.user-c-phone}")
    private String userCPhone;

    /**
     * 用于小程序审核登录B端手机
     */
    @Value("${wx.user-b-phone}")
    private String userBPhone;

    /**
     * 用于小程序审核登录验证码
     */
    @Value("${wx.user-code}")
    private String userCode;

    /**
     * 微信用户校验权限 ON-开启用户检验、OFF-关闭（用于小程序审核)
     */
    @Value("${wx.user-check}")
    private String userCheck;

    //=================================园区=====================================

    /**
     * 小翼园区助手appId
     */
    @Value("${wx.zone.assistant.app-id}")
    private String zoneAssistantAppId;

    /**
     * 小翼园区助手secret
     */
    @Value("${wx.zone.assistant.secret}")
    private String zoneAssistantSecret;

    //=================================校园===================================

    /**
     * 校园appId
     */
    @Value("${wx.campus.app-id}")
    private String campusAppId;

    /**
     * 校园secret
     */
    @Value("${wx.campus.secret}")
    private String campusSecret;


}
