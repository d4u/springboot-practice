package com.springboot.pratice.common.config.redis;

import org.springframework.context.annotation.Configuration;

/**
 * @author yanglingyu
 * @date 2021/10/19
 */
@Configuration
public class RedisConfig {

//    @Bean
//    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
//        RedisTemplate<String, Object> template = new RedisTemplate<>();
//        template.setConnectionFactory(redisConnectionFactory);
//        // key都使用String的方式序列化
//        template.setKeySerializer(RedisSerializer.string());
//        template.setHashKeySerializer(RedisSerializer.string());
//        // value都使用json的方式序列化
//        template.setValueSerializer(RedisSerializer.json());
//        template.setHashValueSerializer(RedisSerializer.json());
//        template.afterPropertiesSet();
//        return template;
//    }
}
