package com.springboot.pratice.common.config.mthread;

import lombok.Data;

/**
 * @author zengjinliang
 * Create on 2021/12/7 16:35
 */
@Data
public class ThreadConfig {
    private String poolName;
    private int corePoolSize;
    private int maximumPoolSize;
    private long keepAliveTime;
    private int allWaitSize;
}
