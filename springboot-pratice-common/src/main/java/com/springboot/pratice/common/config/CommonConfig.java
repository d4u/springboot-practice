package com.springboot.pratice.common.config;

import com.springboot.pratice.common.config.rsa.SecurityUtil;
import com.springboot.pratice.common.config.exception.GlobalExceptionHandler;
import com.springboot.pratice.common.config.mthread.MThreadPoolProperties;
import com.springboot.pratice.common.config.mthread.MThreadInit;
import com.springboot.pratice.common.config.redis.RedisConfig;
import com.springboot.pratice.common.config.redis.RedisUtil;
import com.springboot.pratice.common.config.weixin.WeiXinConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author hgy
 * Create on 2021/08/19 15:50
 */

@Import({
        GlobalExceptionHandler.class,
        RedisConfig.class,
        RedisUtil.class,
        WeiXinConfig.class,
        MThreadInit.class,
        MThreadPoolProperties.class,
        SecurityUtil.class,
        RestTemplateConfig.class
})
@Configuration
public class CommonConfig {

}
