package com.springboot.pratice.common.config.message;

/**
 * @Description: 短信相关参数
 * @Author: hgy
 * @CreateDate: 2021/6/16 11:22
 */
public interface MobileRedisConstant {

    String SMS_LOGIN_ERROR_NUMBER = "scep:sms:login:error:number:";

    /**
     * 同一个手机号最近10分钟发送短信的时间记录
     */
    String SMS_SEND_RECORD = "scep:sms:record:phone:";


    /**
     * 访客手机号验证
     */
    String SMS_SEND_CUSTOMER = "scep:sms:customer:phone:";

    /**
     * SMS_SEND_RECORD 有效时长 10min
     */
    Long SMS_SEND_RECORD_TIME = 60 * 10L;

    /**
     * SMS_SEND_RECORD 就近两次时间间隔大于 60s
     */
    Long SMS_SEND_RECORD_MIN_INTERVAL = 60 * 1000L;

    /**
     * SMS_SEND_TIME_LIST 有效时长 10min
     */
    Integer SMS_SEND_RECORD_MAX_SIZE = 5;

}
