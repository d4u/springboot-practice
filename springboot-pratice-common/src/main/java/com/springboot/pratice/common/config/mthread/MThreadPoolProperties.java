package com.springboot.pratice.common.config.mthread;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zengjinliang
 * Create on 2021/12/7 16:35
 */
@Data
@Component
@ConditionalOnExpression("${thread-pools.enable:false}")
@ConfigurationProperties(prefix = "thread-pools.config")
public class MThreadPoolProperties {
    private ThreadConfig[] pools;
}
