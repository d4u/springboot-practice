package com.springboot.pratice.common.config.redis;

import org.springframework.stereotype.Component;

/**
 * @author yanglingyu
 * @date 2021/10/19
 */
@Component
public class RedisUtil<T> {

//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;
//
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;
//
//    /******************** 通用方法 **************************/
//
//    public Boolean hasKey(String key) {
//        return redisTemplate.hasKey(key);
//    }
//
//    public Boolean delete(String key) {
//        return redisTemplate.delete(key);
//    }
//
//    public Boolean expire(String key, long timeout, TimeUnit timeUnit) {
//        return redisTemplate.expire(key, timeout, timeUnit);
//    }
//
//    //========================== String类型方法 ==============================
//
//    public void set(String key, String value) {
//        stringRedisTemplate.opsForValue().set(key, value);
//    }
//
//    public Boolean setIfAbsent(String key, String value) {
//        return stringRedisTemplate.opsForValue().setIfAbsent(key, value);
//    }
//
//    /**
//     * 上锁
//     *
//     * @param key
//     * @param timeout
//     * @param timeUnit
//     * @return
//     */
//    public Boolean lock(String key, long timeout, TimeUnit timeUnit) {
//        return stringRedisTemplate.opsForValue().setIfAbsent(key, "lock", timeout, timeUnit);
//    }
//
//    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
//        stringRedisTemplate.opsForValue().set(key, value, timeout, timeUnit);
//    }
//
//    public String get(String key) {
//        return stringRedisTemplate.opsForValue().get(key);
//    }
//
//    /**
//     * 从redis中获取key对应的过期时间;
//     * 如果该值有过期时间，就返回相应的过期时间;
//     * 如果该值没有设置过期时间，就返回-1;
//     * 如果没有该值，就返回-2;
//     */
//    public Long getExpire(String key) {
//        return redisTemplate.opsForValue().getOperations().getExpire(key);
//    }
//
//    public void setObject(String key, T value, long timeout, TimeUnit timeUnit) {
//        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
//    }
//
//    public T getObject(String key) {
//        return (T) redisTemplate.opsForValue().get(key);
//    }
//
//    /**
//     * 数值类型的value自增1
//     *
//     * @param key key
//     * @return
//     */
//    public Long increment(String key) {
//        return redisTemplate.opsForValue().increment(key);
//    }
//
//    /**
//     * 获取指定 key 的值并转成相应数组类型，为空时返回null
//     *
//     * @param key
//     * @return
//     */
//    public <T> List<T> getJsonArray(String key, Class<T> clazz) {
//        String cacheObj = get(key);
//        if (StringUtils.isNotBlank(cacheObj)) {
//            return JSONObject.parseArray(cacheObj, clazz);
//        }
//        return null;
//    }
//
//    /**
//     * 将值 value 转成JSON字符串并关联到 key ，并将 key 的过期时间设为 timeout
//     *
//     * @param key
//     * @param value
//     * @param timeout 过期时间
//     * @param unit    时间单位, 天:TimeUnit.DAYS 小时:TimeUnit.HOURS 分钟:TimeUnit.MINUTES
//     *                秒:TimeUnit.SECONDS 毫秒:TimeUnit.MILLISECONDS
//     */
//    public void setJsonStr(String key, Object value, long timeout, TimeUnit unit) {
//        String jsonString = JSON.toJSONString(value);
//        set(key, jsonString, timeout, unit);
//    }
//
//    //========================== List类型方法 ==============================
//
//    /**
//     * 从尾部添加list
//     *
//     * @param key  key
//     * @param list value
//     * @return 添加后redis的list总长度
//     */
//    public Long rightPushAll(String key, List<T> list) {
//        return redisTemplate.opsForList().rightPushAll(key, list.toArray());
//    }
//
//    /**
//     * 从尾部添加list并设置过期时间
//     *
//     * @param key      key
//     * @param list     value
//     * @param timeout  过期时间
//     * @param timeUnit 时间单位
//     * @return 添加后redis的list总长度
//     */
//    public Long rightPushAll(String key, List<T> list, long timeout, TimeUnit timeUnit) {
//        Long size = redisTemplate.opsForList().rightPushAll(key, list.toArray());
//        redisTemplate.expire(key, timeout, timeUnit);
//        return size;
//    }
//
//    /**
//     * 拿到整个list
//     *
//     * @param key key
//     * @return list
//     */
//    public List<T> rangeAll(String key) {
//        return (List<T>) redisTemplate.opsForList().range(key, 0, -1);
//    }
//
//    //========================== Set类型方法 ==============================
//
//    /**
//     * 放入元素到set中
//     *
//     * @param key  key
//     * @param item 元素
//     * @return set的长度
//     */
//    public Long putInSet(String key, T item) {
//        return redisTemplate.opsForSet().add(key, item);
//    }
//
//    /**
//     * 判断某个元素是否在set中
//     *
//     * @param key  key
//     * @param item 元素
//     * @return
//     */
//    public Boolean isMember(String key, T item) {
//        return redisTemplate.opsForSet().isMember(key, item);
//    }
//
//    /**
//     * 拿到set的所有元素
//     *
//     * @param key key
//     * @return
//     */
//    public Set<T> members(String key) {
//        return (Set<T>) redisTemplate.opsForSet().members(key);
//    }
//
//    /**
//     * 从set中移除
//     *
//     * @param key
//     * @param item
//     * @return
//     */
//    public Long removeFromSet(String key, T item) {
//        Long count = redisTemplate.opsForSet().remove(key, item);
//        return count;
//    }
//
//    //=========================== Hash类型方法 ====================================
//
//    /**
//     * 放入hash
//     *
//     * @param key key
//     * @param map hashmap
//     */
//    public void hashPutAll(String key, Map<String, T> map) {
//        redisTemplate.opsForHash().putAll(key, map);
//    }
//
//    /**
//     * 放入hash
//     *
//     * @param key       key
//     * @param hashKey   hash的key
//     * @param hashValue hash的value
//     */
//    public void hashPut(String key, String hashKey, T hashValue) {
//        redisTemplate.opsForHash().put(key, hashKey, hashValue);
//    }
//
//    /**
//     * 从哈希中取值
//     *
//     * @param key     key
//     * @param hashKey hash的key
//     * @return
//     */
//    public T hashGet(String key, String hashKey) {
//        return (T) redisTemplate.opsForHash().get(key, hashKey);
//    }
//
//    /**
//     * 拿到整个hashmap
//     *
//     * @param key key
//     * @return hashmap
//     */
//    public Map<Object, T> hashGetAll(String key) {
//        return (Map<Object, T>) redisTemplate.opsForHash().entries(key);
//    }

}
