package com.springboot.pratice.common.annotation;

import com.springboot.pratice.common.config.CommonConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author hgy
 * Create on 2021/08/19 15:50
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({CommonConfig.class})
@Documented
public @interface EnableCommonConfig {
}
