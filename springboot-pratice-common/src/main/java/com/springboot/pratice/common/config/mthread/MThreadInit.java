package com.springboot.pratice.common.config.mthread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

/**
 * @author zengjinliang
 * Create on 2021/12/7 16:35
 * 初始化数据分类-子类对应的线程池
 */
@Component
@Slf4j
public class MThreadInit {
    @Autowired(required = false)
    MThreadPoolProperties mThreadPoolProperties;

    @PostConstruct
    private void init() {
        if (mThreadPoolProperties != null) {
            Arrays.stream(mThreadPoolProperties.getPools()).forEach(
                    threadConfig -> MThreadManager.create(threadConfig.getPoolName(), threadConfig.getCorePoolSize(), threadConfig.getMaximumPoolSize(),
                            threadConfig.getKeepAliveTime(), threadConfig.getAllWaitSize())
            );
            log.info("MThread init over.");
        } else {
            log.info("no MThread init over.");
        }


    }

//    @Scheduled(cron="0 0/1 * * * ?")
    public void doJob() {
        String monitor = MThreadManager.monitor();
        log.info("MThreadPool monitor state:" + monitor);
    }
}
