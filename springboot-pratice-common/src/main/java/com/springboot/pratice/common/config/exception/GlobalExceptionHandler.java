/*
 * Copyright © 2020 ctwing
 */
package com.springboot.pratice.common.config.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.sql.DataTruncation;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.Set;

/**
 * 全局拦截异常
 *
 * @author 潘星宇 xingyu.pan@luckincoffee.com
 * @date 2020/6/23 10:39
 */
@ControllerAdvice
@Slf4j
@ResponseBody
public class GlobalExceptionHandler {

}
