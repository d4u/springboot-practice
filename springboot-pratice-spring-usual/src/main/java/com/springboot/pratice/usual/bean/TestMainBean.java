package com.springboot.pratice.usual.bean;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author daisy
 * @date 2020/1/8 0:48
 */
public class TestMainBean {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(PrePostConfig.class);
        BeanWayService beanWayService = ctx.getBean(BeanWayService.class);
        JSR250WayService jSR250WayService = ctx.getBean(JSR250WayService.class);

        ctx.close();
    }
}
