package com.springboot.pratice.usual.scope;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author daisy
 * @date 2019/12/22 22:49
 */
@Configuration
@ComponentScan("com.springboot.pratice.usual.scope")
public class ScopeConfig {
}
