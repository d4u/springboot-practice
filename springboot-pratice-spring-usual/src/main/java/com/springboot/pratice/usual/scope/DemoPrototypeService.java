package com.springboot.pratice.usual.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author daisy
 * @date 2019/12/22 22:47
 */
@Service
@Scope("prototype")
public class DemoPrototypeService {
}
