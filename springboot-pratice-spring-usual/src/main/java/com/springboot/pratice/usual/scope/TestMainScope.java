package com.springboot.pratice.usual.scope;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.Assert;


/**
 * @author daisy
 * @date 2019/12/22 22:52
 */
public class TestMainScope {
    public static void main(String[] args) {
        //使用AnnotationConfigApplicationContext作用容器，接收输入一个配置类为参数
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ScopeConfig.class);
        DemoPrototypeService demoPrototypeService1 = applicationContext.getBean(DemoPrototypeService.class);
        DemoPrototypeService demoPrototypeService2 = applicationContext.getBean(DemoPrototypeService.class);

        DemoSingletonService demoSingletonService1 = applicationContext.getBean(DemoSingletonService.class);
        DemoSingletonService demoSingletonService2 = applicationContext.getBean(DemoSingletonService.class);

        System.out.println("demoPrototypeService1.equals(demoPrototypeService2)? "+ demoPrototypeService1.equals(demoPrototypeService2));
        System.out.println("demoSingletonService1.equals(demoSingletonService2)? "+ demoSingletonService1.equals(demoSingletonService2));
        Assert.isTrue(demoPrototypeService1.equals(demoPrototypeService2),"Prototype不是单例");
        Assert.isTrue(demoSingletonService1.equals(demoSingletonService2),"Singleton不是单例");
    }

}
