package com.springboot.pratice.usual.el;

import com.springboot.pratice.usual.scope.ScopeConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author daisy
 * @date 2019/12/25 0:35
 */
public class TestMainEl {
    public static void main(String[] args) {
        //使用AnnotationConfigApplicationContext作用容器，接收输入一个配置类为参数
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ElConfig.class);
        ElConfig config = ctx.getBean(ElConfig.class);
        config.outputResource();
    }
}
