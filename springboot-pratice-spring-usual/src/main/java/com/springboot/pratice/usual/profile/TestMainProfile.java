package com.springboot.pratice.usual.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author daisy
 * @date 2020/1/9 21:39
 */
public class TestMainProfile {
    public static void main(String[] args) {
        test3();

    }

    static void test1(){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("prodd");//活动的Profile设置为prod
        ctx.register(ProfileConfig.class);//后置注册Bean配置类，不然会报Bean未定义的错误
        ctx.refresh();//刷新容器

        DemoBean demoBean = ctx.getBean(DemoBean.class);
        System.out.println(demoBean.getContent());
    }
    static void test2(){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ProfileConfig.class);
        DemoBean demoBean = ctx.getBean(DemoBean.class);
        System.out.println(demoBean.getContent());
    }

    static void test3(){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("test");//活动的Profile设置为prod
        ctx.register(ProfileConfig2.class);//后置注册Bean配置类，不然会报Bean未定义的错误
        ctx.refresh();//刷新容器

        DemoBean demoBean1 = ctx.getBean("demoBean1",DemoBean.class);
        System.out.println(demoBean1.getContent());

        DemoBean demoBean2 = ctx.getBean("demoBean2",DemoBean.class);
        System.out.println(demoBean2.getContent());
    }
}
