package com.springboot.pratice.usual.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author daisy
 * @date 2020/1/9 21:31
 */
@Configuration
public class ProfileConfig {

    @Profile("dev")
    @Bean
    public DemoBean devDemoBean(){
        return new DemoBean("dev profile");
    }

    @Profile("prodd")
    @Bean
    public DemoBean testDemoBean(){
        return new DemoBean("prodd profile");
    }
}
