package com.springboot.pratice.usual.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author daisy
 * @date 2020/1/9 21:31
 */
@Configuration
@Profile("test")
public class ProfileConfig2 {


    @Bean(name="demoBean1")
    public DemoBean getDemoBean1(){
        return new DemoBean("ProfileConfig2 demoBean1");
    }

    @Bean(name="demoBean2")
    public DemoBean getDemoBean2(){
        return new DemoBean("ProfileConfig2 demoBean2");
    }
}
