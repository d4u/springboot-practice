package com.springboot.pratice.usual.el;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author daisy
 * @date 2019/12/25 0:04
 */
@Service
public class DemoElService {
    @Value("其他对象属性")
    private String another;

    public String getAnother() {
        return another;
    }

    public void setAnother(String another) {
        this.another = another;
    }
}
