package com.springboot.pratice.usual.bean;

/**
 * @author daisy
 * @date 2020/1/8 0:25
 */
public class BeanWayService {
    public void init(){
        System.out.println("bean-init-method");
    }

    public BeanWayService() {
        super();
        System.out.println("构造方法-BeanWayService");
    }

    public void destroy(){
        System.out.println("bean-destroy-method");
    }
}
