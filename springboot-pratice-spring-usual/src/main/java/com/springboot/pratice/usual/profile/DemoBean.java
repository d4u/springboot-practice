package com.springboot.pratice.usual.profile;

/**
 * @author daisy
 * @date 2020/1/9 21:32
 */
public class DemoBean {
    private String content;

    public DemoBean(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
