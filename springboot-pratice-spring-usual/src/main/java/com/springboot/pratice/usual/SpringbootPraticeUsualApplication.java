package com.springboot.pratice.usual;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPraticeUsualApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPraticeUsualApplication.class, args);
	}

}
