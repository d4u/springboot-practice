package com.springboot.pratice.usual.bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author daisy
 * @date 2020/1/8 0:33
 */
public class JSR250WayService {
    /**
     * 构造方法执行完之后执行
     */
    @PostConstruct
    public void init(){
        System.out.println("JSR250-init-method");
    }

    public JSR250WayService() {
        super();
        System.out.println("构造方法-JSR250WayService");
    }

    /**
     * 在Bean销毁之前执行
     */
    @PreDestroy
    public void destroy(){
        System.out.println("JSR250-destroy-method");
    }
}
