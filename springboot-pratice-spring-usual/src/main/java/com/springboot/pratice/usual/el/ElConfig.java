package com.springboot.pratice.usual.el;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

/**
 * @author daisy
 * @date 2019/12/25 0:12
 */
@Configuration
@ComponentScan("com.springboot.pratice.usual.el")
@PropertySource("classpath:com.springboot.pratice.usual.el/TestEl.properties")
public class ElConfig {

    @Value("hello,world")
    private String normal;

    /**
     * 操作系统属性
     */
    @Value("#{systemProperties['os.name']}")
    private String osName;
    /**
     * 表达式结果
     */
    @Value("#{T(java.lang.Math).random()*100}")
    private double randomNumber;
    /**
     * 其他Bean属性
     */
    @Value("#{demoElService.another}")
    private String fromAnother;
    /**
     * 文件资源
     */
    @Value("classpath:com.springboot.pratice.usual.el/TestEl.txt")
    private Resource testFile;
    /**
     * 网址资源
     */
    @Value("http://www.baidu.com")
    private Resource testUrl;
    /**
     * 配置文件属性
     */
    @Value("${book.name}")
    private String bookName;

    @Value("${book.author}")
    private String bookAuthor;

    @Autowired
    private Environment env;

//    @Bean
//    public static PropertySourcesPlaceholderConfigurer propertyConfigure(){
//        return new PropertySourcesPlaceholderConfigurer();
//    }

    public void outputResource(){
        try{
            System.out.println("normal:" + normal);
            System.out.println("osName:" + osName);
            System.out.println("randomNumber:" + randomNumber);
            System.out.println("fromAnother:" + fromAnother);
            System.out.println("testFile:" + testFile);
            System.out.println("testUrl:" + testUrl);
            System.out.println("bookName:" + bookName);
            System.out.println("bookAuthor:" + bookAuthor);
            System.out.println("bookAuthor:" + IOUtils.toString(testFile.getInputStream()));
            System.out.println("env[book.author]" + env.getProperty("book.author"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
