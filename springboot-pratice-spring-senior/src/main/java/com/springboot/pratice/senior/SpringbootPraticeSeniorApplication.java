package com.springboot.pratice.senior;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPraticeSeniorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPraticeSeniorApplication.class, args);
	}

}
