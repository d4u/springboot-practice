package com.springboot.pratice.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPraticeCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPraticeCoreApplication.class, args);
	}

}
