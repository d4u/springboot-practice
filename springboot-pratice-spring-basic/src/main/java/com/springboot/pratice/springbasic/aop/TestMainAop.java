package com.springboot.pratice.springbasic.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author daisy
 * @date 2019/12/22 18:53
 */
public class TestMainAop {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);

        DemoAnnotationService annotationService = context.getBean(DemoAnnotationService.class);
        DemoMethodService methodService = context.getBean(DemoMethodService.class);

        annotationService.add();
        annotationService.mul();

        methodService.add();
        methodService.mul();

        context.close();
    }
}
