package com.springboot.pratice.springbasic.javaconfig;

/**
 * 这里没有使用@Service声明一个Bean
 *
 * @author daisy
 * @date 2019/12/22 12:31
 */
public class NoAnnotationFunctionService {
    public String sayHello(String word){
        return "hello," + word +"!";
    }
}
