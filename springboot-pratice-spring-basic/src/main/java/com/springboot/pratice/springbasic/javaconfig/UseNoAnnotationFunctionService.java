package com.springboot.pratice.springbasic.javaconfig;

/**
 * 这里没有使用Autowired注入Bean
 *
 * @author daisy
 * @date 2019/12/22 12:32
 */
public class UseNoAnnotationFunctionService {
    private NoAnnotationFunctionService noAnnotationFunctionService;

    public void setNoAnnotationFunctionService(NoAnnotationFunctionService noAnnotationFunctionService) {
        this.noAnnotationFunctionService = noAnnotationFunctionService;
    }

    public UseNoAnnotationFunctionService(NoAnnotationFunctionService noAnnotationFunctionService) {
        this.noAnnotationFunctionService = noAnnotationFunctionService;
    }

    public String sayHello(String word){
        return noAnnotationFunctionService.sayHello(word);
    }
}
