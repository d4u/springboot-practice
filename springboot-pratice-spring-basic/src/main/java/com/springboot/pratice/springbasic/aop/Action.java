package com.springboot.pratice.springbasic.aop;

import java.lang.annotation.*;

/**
 * 自定义注解
 * @author daisy
 * @date 2019/12/22 18:33
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Action {
    String name();
}
