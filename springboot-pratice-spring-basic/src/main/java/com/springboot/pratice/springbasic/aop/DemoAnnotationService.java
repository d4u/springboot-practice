package com.springboot.pratice.springbasic.aop;

import org.springframework.stereotype.Service;

/**
 * 使用“注解”规则被拦截类
 *
 * @author daisy
 * @date 2019/12/22 18:36
 */
@Service
public class DemoAnnotationService {

    @Action(name = "注解式拦截")
    public void add(){
        System.out.println(this.getClass() + ".add()...");
    }

    public void mul(){
        System.out.println(this.getClass() + ".mul()...");
    }
}
