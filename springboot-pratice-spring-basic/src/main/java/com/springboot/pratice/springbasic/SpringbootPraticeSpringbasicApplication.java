package com.springboot.pratice.springbasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPraticeSpringbasicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootPraticeSpringbasicApplication.class, args);
	}

}
