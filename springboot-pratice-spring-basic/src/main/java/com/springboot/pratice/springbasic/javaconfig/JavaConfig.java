package com.springboot.pratice.springbasic.javaconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置：Java配置
 *
 * @author daisy
 * @date 2019/12/22 12:30
 */
@Configuration
//@ComponentScan("com.springboot.pratice.core")//扫描包下的注解类，并注册为Bean
public class JavaConfig {

    @Bean
    public NoAnnotationFunctionService noAnnotationFunctionService(){
        return new NoAnnotationFunctionService();
    }

    @Bean
    public UseNoAnnotationFunctionService useNoAnnotationFunctionService(NoAnnotationFunctionService noAnnotationFunctionService){
        return new UseNoAnnotationFunctionService(noAnnotationFunctionService);
    }
}
