package com.springboot.pratice.springbasic.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 编写切面
 *1）execution(* *(..))
 * //表示匹配所有方法
 * 2）execution(public * com.springboot.pratice.core.aop.DemoMethodService.*(..))
 * //表示匹配com.springboot.pratice.core.aop.DemoMethodService中所有的公有方法
 * 3）execution(* com.springboot.pratice.core.aop..*.*(..))
 * //表示匹配com.springboot.pratice.core.aop 包及其子包下的所有方法
 *
 * @author daisy
 * @date 2019/12/22 19:21
 */
@Aspect//声明一个切面
@Component // 注册都Spring容器成为Bean
public class DemoAspect {

    @Pointcut("@annotation(com.springboot.pratice.springbasic.aop.Action)")//
    public void annotationPointCut(){};

    @Pointcut("execution(* com.springboot.pratice.springbasic.aop.DemoMethodService.*(..))")
    public void methodPointCut(){}

    @After("annotationPointCut()")
    public void after(JoinPoint jpt){
        MethodSignature sgr = (MethodSignature)jpt.getSignature();
        Method method = sgr.getMethod();
        Action action = method.getAnnotation(Action.class);
        System.out.println("注解式拦截，注解名：" + action.name() + "，方法名："+method.getName());
    }
    @Before("methodPointCut()")
    public void before(JoinPoint jpt){
        MethodSignature sgr = (MethodSignature)jpt.getSignature();
        Method method = sgr.getMethod();
        System.out.println("方法式拦截,方法名：" + method.getName());
    }
}
