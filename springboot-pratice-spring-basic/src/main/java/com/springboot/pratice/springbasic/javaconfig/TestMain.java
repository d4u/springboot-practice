package com.springboot.pratice.springbasic.javaconfig;

import com.springboot.pratice.springbasic.di.DiConfig;
import com.springboot.pratice.springbasic.di.UseFunctionService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author daisy
 * @date 2019/12/22 1:49
 */
public class TestMain {
    public static void main(String[] args) {
        context2();
    }
    static void context1(){
        //使用AnnotationConfigApplicationContext作用容器，接收输入一个配置类为参数
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(DiConfig.class);
        UseFunctionService useFunctionService = applicationContext.getBean(UseFunctionService.class);
        String aa = useFunctionService.sayHello("Daisy");
        System.out.println(aa);
    }

    static void context2(){
        //使用AnnotationConfigApplicationContext作用容器，接收输入一个配置类为参数
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(JavaConfig.class);
        UseNoAnnotationFunctionService useNoAnnotationFunctionService = applicationContext.getBean(UseNoAnnotationFunctionService.class);
        String aa = useNoAnnotationFunctionService.sayHello("Daisy");
        System.out.println(aa);
    }
}
