package com.springboot.pratice.springbasic.aop;

import org.springframework.stereotype.Service;

/**
 * 使用“方法”规则被拦截类
 *
 * @author daisy
 * @date 2019/12/22 18:38
 */
@Service
public class DemoMethodService {
    public void add(){
        System.out.println(this.getClass() + ".add()...");
    }
    public void mul(){
        System.out.println(this.getClass() + ".mul()...");
    }
}
