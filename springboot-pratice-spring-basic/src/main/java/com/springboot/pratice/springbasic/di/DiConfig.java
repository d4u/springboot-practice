package com.springboot.pratice.springbasic.di;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 配置：依赖注入
 */
@Configuration//声明为一个配置类
@ComponentScan("com.springboot.pratice.springbasic.di")//扫描包下的注解类，并注册为Bean
public class DiConfig {
}
