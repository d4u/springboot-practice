package com.springboot.pratice.springbasic.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author daisy
 * @date 2019/12/22 18:54
 */
@Configuration
@ComponentScan("com.springboot.pratice.springbasic.aop")
@EnableAspectJAutoProxy//开启Spring 对AspectJ代理的支持
public class AopConfig {

}
