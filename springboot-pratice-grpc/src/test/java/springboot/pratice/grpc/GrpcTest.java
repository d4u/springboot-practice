package springboot.pratice.grpc;

import org.junit.jupiter.api.Test;
import springboot.pratice.grpc.client.HelloWorldClient;
import springboot.pratice.grpc.server.HelloWorldServer;

/**
 * gRPC测试用例
 * @author daisy
 * @date 2020/5/20 14:44
 */
public class GrpcTest {

    @Test
    public void startServer() throws Exception {
        HelloWorldServer.startServer();
    }

    @Test
    public void startClient() throws Exception {
        HelloWorldClient.start();
    }
}
